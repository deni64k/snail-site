window.cdn_local = "192.168.0.224"
window.cdn = (window.location.hostname == "localhost"  ||
              window.location.hostname == "babe"       ||
              window.location.hostname == "skelfiskur" ||
              window.location.hostname == "127.0.0.1"  ||
              window.location.hostname == cdn_local)
           ? cdn_local
           : "negval.starlink.ru";
window.size_current = window.location.hash.slice(1) || "hd720";
window.size_current = (window.size_current != "hd720" &&
                       window.size_current != "hd480")
           ? "hd720"
           : window.size_current;

window.sources_abbrs = {
  "hd1080": [
    {
      file: "http://" + window.cdn + "/hls/snail-hd1080.m3u8",
      label: "1080p HD"
    }, {
      file: "rtmp://" + window.cdn + "/live/flv:snail-hd1080",
      label: "1080p HD"
    }
  ],
  "hd720": [
    {
    //   file: "http://" + window.cdn + ":60789/snail-720p.webm",
    //   label: "720p HD"
    // }, {
      file: "http://" + window.cdn + "/hls/snail-hd720.m3u8",
      label: "720p HD"
    }, {
      file: "rtmp://" + window.cdn + "/live/flv:snail-hd720",
      label: "720p HD"
    }
  ],
  "hd480": [
    {
    //   file: "http://" + window.cdn + ":60789/snail-480p.webm",
    //   label: "480p HD"
    // }, {
      file: "http://" + window.cdn + "/hls/snail-hd480.m3u8",
      label: "480p HD"
    }, {
      file: "rtmp://" + window.cdn + "/live/flv:snail-hd480",
      label: "480p HD"
    }
  ]
}
window.size_abbrs = {
  "hd1080": {width: 1920, height: 1080},
  "hd720":  {width: 1280, height: 720},
  "hd480":  {width:  852, height: 480}
}

window.setup_player = function (abbr) {
  jwplayer("snail-video").setup({
    playlist: [{
      title: "My lovely snail",
      image: "img/cover.png",
      sources: window.sources_abbrs[abbr]
    }],
    rtmp: {
      bufferlength: 4
    },
    width: window.size_abbrs[abbr]["width"],
    height: window.size_abbrs[abbr]["height"],
    autostart: true,
    mute: true,
    screencolor: 'FFFFFF',
    wmode: 'transparent'
  });

  $("#resolution-switcher .btn:has([id=" + abbr + "])").button('toggle');
}

setup_player(window.size_current);
jwplayer("snail-video").setVolume(40);

$("#resolution-switcher .btn").click(function(ev) {
  abbr = ev.currentTarget.querySelector("[name=resolution]").id
  window.location.hash = abbr;
  setup_player(abbr);
});